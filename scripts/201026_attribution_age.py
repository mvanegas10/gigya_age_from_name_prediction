#!/usr/bin/env python
# coding: utf-8


import pandas as pd
import numpy as np
import unidecode
import datetime
import random
import re
import sqlalchemy as sql
import configparser
import sys


# ## Config


config = configparser.ConfigParser()
config.read('../config/default.ini')

engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(config['DATABASE']['username'], config['DATABASE']['password'].replace('_percentage_', '%'), config['DATABASE']['host'], config['DATABASE']['port'], config['DATABASE']['database']))

ids = {'actito': 'actitoid', 'gigya': 'uid'}

# ## Utils


regex = re.compile('[^a-zA-Z]')

def to_camel_case(string):
    string = string.strip()
    if ' ' in  string:
        names = []
        for x in string.split(' '):
            x = x.strip()
            names.append('{}{}'.format(x[:1].upper(), x[1:].lower()))
        return ' '.join(names)
    elif '-' in  string:
        names = []
        for x in string.split('-'):
            x = x.strip()
            names.append('{}{}'.format(x[:1].upper(), x[1:].lower()))
        return '-'.join(names)
    else:
        return '{}{}'.format(string[:1].upper(), string[1:].lower())

def to_lower_strip(string):
    string = string.lower().strip()
    return string

def rm_sc(string):
    string = string.lower().strip()
    string = unidecode.unidecode(string).strip()
    string = regex.sub('', string).strip()
    return string

def remove_special_chars(string):
    if not '�' in string:
        strings = string.split('-')
        return '-'.join([rm_sc(x) for x in strings])
    else:
        return string

def remove_email_domain(string):
    if '@' in string:
        return to_lower_strip(string.split('@')[0])
    
def create_substring(row, col):
    length = len(row[col]) if row[col] != None else 0
    
    for i in range(length-1):
        name = row[col][0:length-i]
        if name in firstnames_wo_sc:
            row['found'] = 1
            row['accepted_firstname'] = firstnames_wo_sc[name]['firstname']
            row['possible_genders'] = assign_possible_genders(name, firstnames_wo_sc)
            return row
        elif remove_special_chars(name) in firstnames_wo_sc:
            row['found'] = 1
            row['accepted_firstname'] = firstnames_wo_sc[remove_special_chars(name)]['firstname']
            row['possible_genders'] = assign_possible_genders(remove_special_chars(name), firstnames_wo_sc)
            return row            
        
    for i in range(2, length):
        name = row[col][i:length]
        if name in firstnames_wo_sc:
            row['found'] = 1
            row['accepted_firstname'] = firstnames_wo_sc[name]['firstname']
            row['possible_genders'] = assign_possible_genders(name, firstnames_wo_sc)
            return row
        elif remove_special_chars(name) in firstnames_wo_sc:
            row['found'] = 1
            row['accepted_firstname'] = firstnames_wo_sc[remove_special_chars(name)]['firstname']
            row['possible_genders'] = assign_possible_genders(remove_special_chars(name), firstnames_wo_sc)
            return row         
        
    return row

def find_substrings(row, col):
    string = row[col] if row[col] != None else ''
    accepted = []
    genders = []
    ages = []
    
    if ' ' in string:
        for name in string.split(' '):
            if name in firstnames:
                accepted.append(firstnames[name]['firstname'])
                genders.append(assign_possible_genders(name, firstnames))
                age = assign_possible_ages(name, firstnames)
                if age is not None:
                    ages.append(age)
            elif name in firstnames_wo_sc:
                accepted.append(firstnames_wo_sc[name]['firstname'])
                genders.append(assign_possible_genders(name, firstnames_wo_sc))
                age = assign_possible_ages(name, firstnames_wo_sc)
                if age is not None:
                    ages.append(age)
            elif remove_special_chars(name) in firstnames_wo_sc:
                accepted.append(firstnames_wo_sc[remove_special_chars(name)]['firstname'])
                genders.append(assign_possible_genders(remove_special_chars(name), firstnames_wo_sc))
                age = assign_possible_ages(remove_special_chars(name), firstnames_wo_sc)
                if age is not None:
                    ages.append(age)
            
    if '-' in string:
        for name in string.split('-'):
            if name in firstnames:
                accepted.append(firstnames[name]['firstname'])
                genders.append(assign_possible_genders(name, firstnames))
                age = assign_possible_ages(name, firstnames)
                if age is not None:
                    ages.append(age)
            elif name in firstnames_wo_sc:
                accepted.append(firstnames_wo_sc[name]['firstname'])
                genders.append(assign_possible_genders(name, firstnames_wo_sc))
                age = assign_possible_ages(name, firstnames_wo_sc)
                if age is not None:
                    ages.append(age)
            elif remove_special_chars(name) in firstnames_wo_sc:
                accepted.append(firstnames_wo_sc[remove_special_chars(name)]['firstname'])
                genders.append(assign_possible_genders(remove_special_chars(name), firstnames_wo_sc))
                age = assign_possible_ages(remove_special_chars(name), firstnames_wo_sc)
                if age is not None:
                    ages.append(age)
            
    if '.' in string:
        for name in string.split('.'):
            if name in firstnames:
                accepted.append(firstnames[name]['firstname'])
                genders.append(assign_possible_genders(name, firstnames))
                age = assign_possible_ages(name, firstnames)
                if age is not None:
                    ages.append(age)
            elif name in firstnames_wo_sc:
                accepted.append(firstnames_wo_sc[name]['firstname'])
                genders.append(assign_possible_genders(name, firstnames_wo_sc))
                age = assign_possible_ages(name, firstnames_wo_sc)
                if age is not None:
                    ages.append(age)
            elif remove_special_chars(name) in firstnames_wo_sc:
                accepted.append(firstnames_wo_sc[remove_special_chars(name)]['firstname'])  
                genders.append(assign_possible_genders(remove_special_chars(name), firstnames_wo_sc))
                age = assign_possible_ages(remove_special_chars(name), firstnames_wo_sc)
                if age is not None:
                    ages.append(age)
            
    if '_' in string:
        for name in string.split('_'):
            if name in firstnames:
                accepted.append(firstnames[name]['firstname'])
                genders.append(assign_possible_genders(name, firstnames))
                age = assign_possible_ages(name, firstnames)
                if age is not None:
                    ages.append(age)
            elif name in firstnames_wo_sc:
                accepted.append(firstnames_wo_sc[name]['firstname'])
                genders.append(assign_possible_genders(name, firstnames_wo_sc))
                age = assign_possible_ages(name, firstnames_wo_sc)
                if age is not None:
                    ages.append(age)
            elif remove_special_chars(name) in firstnames_wo_sc:
                accepted.append(firstnames_wo_sc[remove_special_chars(name)]['firstname'])
                genders.append(assign_possible_genders(remove_special_chars(name), firstnames_wo_sc))
                age = assign_possible_ages(remove_special_chars(name), firstnames_wo_sc)
                if age is not None:
                    ages.append(age)
                
    if '#' in string:
        for name in string.split('#'):
            if name in firstnames:
                accepted.append(firstnames[name]['firstname'])
                genders.append(assign_possible_genders(name, firstnames))
                age = assign_possible_ages(name, firstnames)
                if age is not None:
                    ages.append(age)
            elif name in firstnames_wo_sc:
                accepted.append(firstnames_wo_sc[name]['firstname'])
                genders.append(assign_possible_genders(name, firstnames_wo_sc))
                age = assign_possible_ages(name, firstnames_wo_sc)
                if age is not None:
                    ages.append(age)
            elif remove_special_chars(name) in firstnames_wo_sc:
                accepted.append(firstnames_wo_sc[remove_special_chars(name)]['firstname'])                
                genders.append(assign_possible_genders(remove_special_chars(name), firstnames_wo_sc))
                age = assign_possible_ages(remove_special_chars(name), firstnames_wo_sc)
                if age is not None:
                    ages.append(age)

    if len(accepted) > 0:
        row['found'] = 1
        row['accepted_firstname'] = ' '.join(accepted)
        row['possible_genders'] = ','.join(genders)
        row['possible_ages'] = ','.join(ages)
    return row

def assign_possible_genders(name, obj):
    rnd = random.random()
    if rnd < obj[name]['m']:
        return 'm'
    else:
        return 'f'

def assign_possible_ages(name, obj):
    rnd = random.random()
    if rnd < obj[name]['dif'] <= 20:
        return '{}-{}'.format(obj[name]['min'],obj[name]['max'])

def choose_gender(string):
    if string == string and ',' in string:
        values = string.split(',')
        m = sum([1 for m in values if m == 'm'])
        f = sum([1 for f in values if f == 'f'])
        if m > f:
            return 'm'
        elif m < f:
            return 'f'
        else:
            return np.nan
    else:
        return string


# ## Variables et constants


actito = 'actito'
gigya = 'gigya'



table = actito
#table = gigya


# ## Données

# ### Prénoms provenant de [Statbel](https://statbel.fgov.be/fr/themes/population/noms-et-prenoms/prenoms-filles-et-garcons#panel-13) et [Insee](https://www.insee.fr/fr/statistiques/2540004)


statbel = '../input/names_gender_age_belgique.csv'
insee = '../input/names_gender_age_france.csv'



df_firstnames_sb = pd.read_csv(statbel)
df_firstnames_sb['source'] = 'statbel'
df_firstnames_sb.head()



df_firstnames_in = pd.read_csv(insee)
df_firstnames_in['source'] = 'insee'
df_firstnames_in.head()



df_firstnames = df_firstnames_in.append(df_firstnames_sb, ignore_index=True)
df_firstnames['m'] = df_firstnames['m'].apply(lambda x: x if x == x else 0)
df_firstnames['f'] = df_firstnames['f'].apply(lambda x: x if x == x else 0)
df_firstnames = df_firstnames[df_firstnames['firstname'] != 'Nan']
df_firstnames.shape



df_firstnames['total'] = df_firstnames['m'] + df_firstnames['f']
df_firstnames['m'] = df_firstnames['m']/df_firstnames['total']
df_firstnames['f'] = df_firstnames['f']/df_firstnames['total']
del df_firstnames['total']
df_firstnames.head()



firstnames = {}
for i, row in df_firstnames.iterrows():
    if row['firstname'] == row['firstname']:
        if to_lower_strip(row['firstname']) in firstnames:
            min_val = firstnames[to_lower_strip(row['firstname'])]['min']         
            max_val = firstnames[to_lower_strip(row['firstname'])]['max']         
        firstnames[to_lower_strip(row['firstname'])] = row.to_dict()
        if min_val is not None:
            firstnames[to_lower_strip(row['firstname'])]['min'] = min_val
        if max_val is not None:
            firstnames[to_lower_strip(row['firstname'])]['min'] = max_val



firstnames_wo_sc = {}
for i, row in df_firstnames.iterrows():
    if row['firstname'] == row['firstname']:
        if remove_special_chars(row['firstname']) in firstnames_wo_sc:
            min_val = firstnames_wo_sc[remove_special_chars(row['firstname'])]['min']         
            max_val = firstnames_wo_sc[remove_special_chars(row['firstname'])]['max']         
        firstnames_wo_sc[remove_special_chars(row['firstname'])] = row.to_dict()
        if min_val is not None:
            firstnames_wo_sc[remove_special_chars(row['firstname'])]['min'] = min_val
        if max_val is not None:
            firstnames_wo_sc[remove_special_chars(row['firstname'])]['min'] = max_val        



firstnames_er_sc = {}
for i, row in df_firstnames.iterrows():
    if row['firstname'] == row['firstname']:
        if '{}'.format(re.sub('[^A-Za-z0-9]+', '', to_lower_strip(row['firstname']))) in firstnames_er_sc:
            min_val = firstnames_er_sc['{}'.format(re.sub('[^A-Za-z0-9]+', '', to_lower_strip(row['firstname'])))]['min']         
            max_val = firstnames_er_sc['{}'.format(re.sub('[^A-Za-z0-9]+', '', to_lower_strip(row['firstname'])))]['max']         
        firstnames_er_sc['{}'.format(re.sub('[^A-Za-z0-9]+', '', to_lower_strip(row['firstname'])))] = row.to_dict()
        if min_val is not None:
            firstnames_er_sc['{}'.format(re.sub('[^A-Za-z0-9]+', '', to_lower_strip(row['firstname'])))]['min'] = min_val
        if max_val is not None:
            firstnames_er_sc['{}'.format(re.sub('[^A-Za-z0-9]+', '', to_lower_strip(row['firstname'])))]['min'] = max_val          


def process_data(df_users):
    if len(df_users) == 0:
        return pd.DataFrame(columns=['actitoid', 'assignedfirstname', 'assignedlastname', 'acquisition', 'possible_genders'])
    # ### Utilisateurs

    df_users['original_firstname'] = df_users['firstname']
    df_users['original_lastname'] = df_users['lastname']
    df_users['firstname'] = df_users['firstname'].astype(str)
    df_users['lastname'] = df_users['lastname'].astype(str)
    df_users['found'] = 0
    df_users['accepted_firstname'] = ''
    df_users['acquisition'] = ''
    df_users['possible_ages'] = ''
    df_users['possible_genders'] = ''

    df_users['firstname'] = df_users['firstname'].apply(lambda x: to_lower_strip(x))
    df_users['lastname'] = df_users['lastname'].apply(lambda x: to_lower_strip(x))

    # 
    # 

    # ### 1.1 Attribution de prénom à partir du champ "firstname"


    df_working_ds = df_users[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')]


    df_working_ds['accepted_firstname'] = df_working_ds['firstname'].apply(lambda x: firstnames[x]['firstname'] if x in firstnames else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if x != '' else 0)
    df_to_update = df_working_ds[df_working_ds['found'] == 1]
    df_to_update['acquisition'] = '1.1'
    df_to_update['possible_genders'] = df_to_update['firstname'].apply(lambda x: assign_possible_genders(x, firstnames))
    df_to_update['possible_ages'] = df_to_update['firstname'].apply(lambda x: assign_possible_ages(x, firstnames))
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 1.1\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 1.2 Attribution de prénom à partir du champ "firstname" en joignant les mots par un tiret


    df_working_ds = df_users[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')]

    df_working_ds['accepted_firstname'] = df_working_ds['firstname'].apply(lambda x: firstnames['-'.join(x.split(' '))]['firstname'] if '-'.join(x.split(' ')) in firstnames else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if x != '' else 0)
    df_to_update = df_working_ds[df_working_ds['found'] == 1]
    df_to_update['acquisition'] = '1.2'
    df_to_update['possible_genders'] = df_to_update['firstname'].apply(lambda x: assign_possible_genders('-'.join(x.split(' ')), firstnames))
    df_to_update['possible_ages'] = df_to_update['firstname'].apply(lambda x: assign_possible_ages('-'.join(x.split(' ')), firstnames))
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 1.2\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 1.3 Attribution de prénom à partir du champ "firstname" + "lastname"


    df_working_ds = df_users[(df_users['found'] == 0)]

    df_working_ds['accepted_firstname'] = df_working_ds.apply(lambda x: firstnames['{}{}'.format(x['firstname'], x['lastname'])]['firstname'] if '{}{}'.format(x['firstname'], x['lastname']) in firstnames else '', axis=1)
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if len(x) > 2 else 0)
    df_to_update = df_working_ds[df_working_ds['found'] == 1]
    df_to_update['acquisition'] = '1.3'
    df_to_update['possible_genders'] = df_to_update.apply(lambda x: assign_possible_genders('{}{}'.format(x['firstname'], x['lastname']), firstnames), axis=1)
    df_to_update['possible_ages'] = df_to_update.apply(lambda x: assign_possible_ages('{}{}'.format(x['firstname'], x['lastname']), firstnames), axis=1)
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 1.3\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))

    # ### 1.4 Attribution de prénom à partir du champ "lastname"


    df_working_ds = df_users[(df_users['found'] == 0) & (df_users['lastname'] != 'nan')]

    df_working_ds['accepted_firstname'] = df_working_ds['lastname'].apply(lambda x: firstnames[x]['firstname'] if x in firstnames else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if len(x) > 2 else 0)
    df_to_update = df_working_ds[df_working_ds['found'] == 1]
    df_to_update['acquisition'] = '1.4'
    df_to_update['possible_genders'] = df_to_update['lastname'].apply(lambda x: assign_possible_genders(x, firstnames))
    df_to_update['possible_ages'] = df_to_update['lastname'].apply(lambda x: assign_possible_ages(x, firstnames))
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 1.4\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 2.1 Attribution du prénom à partir de champ "firstname" en supprimant les caractères spéciaux


    df_working_ds = df_users[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')]

    df_working_ds['accepted_firstname'] = df_working_ds['firstname'].apply(lambda x: firstnames_wo_sc[x]['firstname'] if x in firstnames_wo_sc else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if len(x) > 2 else 0)
    df_to_update = df_working_ds[df_working_ds['found'] == 1]
    df_to_update['acquisition'] = '2.1'
    df_to_update['possible_genders'] = df_to_update['firstname'].apply(lambda x: assign_possible_genders(x, firstnames_wo_sc))
    df_to_update['possible_ages'] = df_to_update['firstname'].apply(lambda x: assign_possible_ages(x, firstnames_wo_sc))
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 2.1\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 2.2 Attribution du prénom à partir de champ "lastname" en supprimant les caractères spéciaux


    df_working_ds = df_users[(df_users['found'] == 0) & (df_users['lastname'] != 'nan')]

    df_working_ds['accepted_firstname'] = df_working_ds['lastname'].apply(lambda x: firstnames_wo_sc[x]['firstname'] if x in firstnames_wo_sc else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if len(x) > 3 else 0)
    df_to_update = df_working_ds[df_working_ds['found'] == 1]
    df_to_update['acquisition'] = '2.2'
    df_to_update['possible_genders'] = df_to_update['lastname'].apply(lambda x: assign_possible_genders(x, firstnames_wo_sc))
    df_to_update['possible_ages'] = df_to_update['lastname'].apply(lambda x: assign_possible_ages(x, firstnames_wo_sc))
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 2.2\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 3.1 Attribution du prénom à partir de champ "firstname" en supprimant les caractères spéciaux


    df_working_ds = df_users[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')]

    df_working_ds['accepted_firstname'] = df_working_ds['firstname'].apply(lambda x: firstnames_wo_sc[remove_special_chars(x)]['firstname'] if remove_special_chars(x) in firstnames_wo_sc else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if len(x) > 2 else 0)
    df_to_update = df_working_ds[df_working_ds['found'] == 1]
    df_to_update['acquisition'] = '3.1'
    df_to_update['possible_genders'] = df_to_update['firstname'].apply(lambda x: assign_possible_genders(remove_special_chars(x), firstnames_wo_sc))
    df_to_update['possible_ages'] = df_to_update['firstname'].apply(lambda x: assign_possible_ages(remove_special_chars(x), firstnames_wo_sc))
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 3.1\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 3.2 Attribution du prénom à partir de champ "lastname" en supprimant les caractères spéciaux


    df_working_ds = df_users[(df_users['found'] == 0) & (df_users['lastname'] != 'nan')]

    df_working_ds['accepted_firstname'] = df_working_ds['lastname'].apply(lambda x: firstnames_wo_sc[remove_special_chars(x)]['firstname'] if remove_special_chars(x) in firstnames_wo_sc else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if len(x) > 2 else 0)
    df_to_update = df_working_ds[df_working_ds['found'] == 1]
    df_to_update['acquisition'] = '3.2'
    df_to_update['possible_genders'] = df_to_update['lastname'].apply(lambda x: assign_possible_genders(remove_special_chars(x), firstnames_wo_sc))
    df_to_update['possible_ages'] = df_to_update['lastname'].apply(lambda x: assign_possible_ages(remove_special_chars(x), firstnames_wo_sc))
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 3.2\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 4.1 Attribution du prénom à partir de un des mots dans le champ "firstname" 


    df_working_ds = df_users[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')]

    df_working_ds = df_working_ds.apply(lambda x: find_substrings(x, 'firstname'), axis=1)
    df_to_update = df_working_ds[df_working_ds['accepted_firstname'].str.len() > 3]
    df_to_update['acquisition'] = '4.1'
    df_working_ds = df_to_update.copy()

    df_users.update(df_working_ds)
    print('\n{}\nAQC 4.1\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 4.2 Attribution du prénom à partir de un des mots dans le champ "lastname" 


    df_working_ds = df_users[(df_users['found'] == 0) & (df_users['lastname'] != 'nan')]

    df_working_ds = df_working_ds.apply(lambda x: find_substrings(x, 'lastname'), axis=1)
    df_to_update = df_working_ds[df_working_ds['accepted_firstname'].str.len() > 3]
    df_to_update['acquisition'] = '4.2'
    df_working_ds = df_to_update.copy()

    df_users.update(df_working_ds)
    print('\n{}\nAQC 4.2\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 5 Attribution du prénom à partir de un des mots dans le champ "email" 


    df_working_ds = df_users[df_users['found'] == 0]

    df_working_ds['tmp'] = df_working_ds['email'].apply(lambda x: remove_email_domain(str(x)))
    df_working_ds = df_working_ds.apply(lambda x: find_substrings(x, 'tmp'), axis=1)
    del df_working_ds['tmp']
    df_to_update = df_working_ds[df_working_ds['accepted_firstname'].str.len() > 3]
    df_to_update['acquisition'] = '5'
    df_working_ds = df_to_update.copy()

    df_users.update(df_working_ds)
    print('\n{}\nAQC 5\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # # ### 6 Attribution du prénom à partir de un sous-chaîne du champ "email" 


    # df_working_ds = df_users[df_users['found'] == 0]

    # df_working_ds['tmp'] = df_working_ds['email'].apply(lambda x: remove_email_domain(str(x)))
    # df_working_ds = df_working_ds.apply(lambda x: create_substring(x, 'tmp'), axis=1)
    # del df_working_ds['tmp']
    # df_to_update = df_working_ds[df_working_ds['accepted_firstname'].str.len() > 3]
    # df_to_update['acquisition'] = '6'
    # df_working_ds = df_to_update.copy()

    # df_users.update(df_working_ds)
    # print('\n{}\nAQC 6\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # # ### 7 Attribution de prénom à partir du champ "firstname" avec erreurs de codification


    # df_working_ds = df_users[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')]

    # df_working_ds['accepted_firstname'] = df_working_ds['firstname'].apply(lambda x: firstnames_er_sc[re.sub('[^A-Za-z0-9]+', '', x)]['firstname'] if re.sub('[^A-Za-z0-9]+', '', x) in firstnames_er_sc and len(firstnames_er_sc[re.sub('[^A-Za-z0-9]+', '', x)]['firstname']) == len(x) else '')
    # df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if x != '' else 0)
    # df_to_update = df_working_ds[df_working_ds['found'] == 1]
    # df_to_update['possible_genders'] = df_to_update['firstname'].apply(lambda x: assign_possible_genders(re.sub('[^A-Za-z0-9]+', '', x), firstnames_er_sc))
    # df_to_update['acquisition'] = '7'
    # df_working_ds.update(df_to_update)

    # df_users.update(df_working_ds)
    # print('\n{}\nAQC 7\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # # ### 8 Attribution du prénom à partir de un sous-chaîne du champ "firstname" 


    # df_working_ds = df_users[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')]

    # df_working_ds = df_working_ds.apply(lambda x: create_substring(x, 'firstname'), axis=1)
    # df_to_update = df_working_ds[df_working_ds['accepted_firstname'].str.len() > 3]
    # df_to_update['found'] = 1
    # df_to_update['acquisition'] = '8'
    # df_working_ds = df_to_update.copy()

    # df_users.update(df_working_ds)
    # print('\n{}\nAQC 8\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ---

    # ## Remplacer valeurs originals pour les champs "firstname" et "lastname"
    # Les champs "original_firstname" et "original_lastname" préservent les valeurs originals


    df_fr_lastname = df_users[df_users['acquisition'].isin(['1.4', '2.2', '3.2', '4.2'])]
    df_fr_lastname['lastname'] = df_fr_lastname['firstname']
    df_users.update(df_fr_lastname)
    df_fr_lastname.head()



    df_users['assignedfirstname'] = df_users['accepted_firstname']
    df_users['assignedlastname'] = df_users['lastname'].apply(to_camel_case)



    tmp = df_users[df_users['found'] == 0]
    tmp['assignedfirstname'] = ''
    tmp['assignedlastname'] = ''
    df_users.update(tmp)



    tmp = df_users[df_users['firstname'] == 'Nan']
    tmp['assignedfirstname'] = ''
    df_users.update(tmp)



    tmp = df_users[df_users['lastname'] == 'Nan']
    tmp['assignedlastname'] = ''
    df_users.update(tmp)


    return df_users[['actitoid', 'assignedfirstname', 'assignedlastname', 'acquisition', 'possible_genders','possible_ages']]


if __name__ == "__main__":
    df = pd.DataFrame()
    table = sys.argv[1]
    idcol = ''
    today = datetime.datetime.strftime(datetime.datetime.now(), '%y-%m-%d')

    if table == 'actito':
        
        df_users = pd.read_sql_query("""SELECT actitoid, email, firstname, lastname FROM egos_actito WHERE assignedfirstname IS NULL""", engine)
        print('\n{}\nUsers {} ({}): {}\n{}\n'.format(''.join('-'*50),table, today, df_users.shape[0],''.join('-'*50)))
        data_actito = process_data(df_users)
        data_actito = data_actito.set_index('actitoid')
        not_null = data_actito[data_actito['assignedfirstname'] != '']['actitoid'].astype(str)

        df_users = pd.read_sql_query("""SELECT gigya.actitoid, email, firstname, lastname FROM egos_gigya gigya INNER JOIN (SELECT actitoid FROM egos_actito WHERE assignedfirstname IS NULL AND actitoid NOT IN ('{}')) actito ON gigya.actitoid = actito.actitoid""".format("', '".join(not_null)), engine)
        print('\n{}\nAdditional users from gigya ({}): {}\n{}\n'.format(''.join('-'*50), today, df_users.shape[0],''.join('-'*50)))
        data_gigya = process_data(df_users)
        data_gigya = data_gigya.set_index('actitoid')

        data_actito.update(data_gigya)
        df = data_actito.copy().reset_index()


    elif table == 'gigya':
        df_not_null = pd.read_sql_query("""SELECT gigya.uid, actito.assignedfirstname, actito.assignedlastname, actito.assignedgender FROM egos_gigya gigya INNER JOIN egos_actito actito ON gigya.actitoid = actito.actitoid WHERE actito.assignedfirstname IS NOT NULL AND actito.assignedgender IS NOT NULL""", engine)
        not_null = list(df_not_null['uid'])
        df_users = pd.read_sql_query("""SELECT uid AS actitoid, email, firstname, lastname FROM egos_gigya""", engine)
        print('\n{}\nUsers {} ({}): {}\n{}\n'.format(''.join('-'*50),table, today, df_users.shape[0],''.join('-'*50)))
        df_users = df_users[~df_users['actitoid'].isin(not_null)]
        data_gigya = process_data(df_users)
        data_gigya = data_gigya.set_index('actitoid')

        not_null.extend(list(data_gigya.index))

        df_users = pd.read_sql_query("""SELECT gigya.uid AS actitoid, actito.email, actito.firstname, actito.lastname FROM egos_gigya gigya INNER JOIN egos_actito actito ON gigya.actitoid = actito.actitoid""", engine)
        df_users = df_users[~df_users['actitoid'].isin(not_null)]
        print('\n{}\nAdditional users from actito ({}): {}\n{}\n'.format(''.join('-'*50), today, df_users.shape[0],''.join('-'*50)))
        data_actito = process_data(df_users)
        data_actito = data_actito.set_index('actitoid')
        data_actito['assignedfirstname'] = data_actito['assignedfirstname'].apply(lambda x: np.nan() if x == '' else x) 
        data_actito['assignedlastname'] = data_actito['assignedlastname'].apply(lambda x: np.nan() if x == '' else x)

        data_gigya.update(data_actito)        
        df = data_gigya.copy().reset_index().rename(columns={'actitoid': 'uid'})

    df['assignedgender'] = df['possible_genders'].apply(choose_gender)
    df.to_csv('output/{}_{}_attribution_firstname.csv'.format(datetime.datetime.strftime(datetime.datetime.now(), '%y%m%d'), table), index=True, encoding='utf-8-sig')

    df[[ids[table], 'assignedfirstname', 'assignedlastname', 'assignedgender','possible_ages']].to_csv('output/{}_{}_attribution_firstname.csv'.format(datetime.datetime.strftime(datetime.datetime.now(), '%y%m%d'), table), index=False, encoding='utf-8-sig')
