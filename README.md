# Gigya age from name prediction

This project intends to predict an user's age based on his first name using Insaa and Statbel births statistics.

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

In order to deploy the application, you need to install the prerequisites listen in the requirements.txt  file. Do the following:
* python (>= 3.6.7 required)
* pip or conda

### Installation

#### 1. Clone the repo
```sh
git clone git@gitlab.com:mvanegas10/gigya_age_from_name_prediction.git
```
#### 2. Install PIP packages
```sh
# Using conda
cd gigya_age_from_name_prediction/
conda create --name <env> python=3.6 --file requirements.txt
conda activate <env>

# Using pip
cd gigya_age_from_name_prediction/
python -m venv <env>

# Windows
<env>\Scripts\activate.bat
# Mac or linux
source <env>/bin/activate

pip install -r requirements.txt
```
### Configuration file 
Create a configuration file:
```sh
nano config/default.ini
```

Modify the file config/default.ini including the database required information:
```ini
[DATABASE]
host = <host>
port = <port>
database = <database>
username = <user>
password = <password>
```

### Running the script
Run the script for a given table. Possible table values: actito or gigya.
```sh
cd scripts
python 201026_attribution_age.py <table>
```
If the script finished the excecution sucessfully, you should find the file in the ```output/``` folder